#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <time.h>
#include <err.h>
#include <curses.h>

struct result_list {
	struct result_list *prev;
	struct result_list *next;
	uint8_t result;
};

enum error_type alloc_next_item(struct result_list *list)
{
	list->next = (struct result_list *) malloc(sizeof(struct result_list));
	if (list->next == NULL) {
		printf("Out of memory\n");
		return ERR_OUT_OF_MEMORY;
	}

	list->next->prev = list;
	
	return ERR_OK;
}

void free_list(struct result_list *list)
{
	struct result_list *buf;
	while(true) {
		buf = list->prev;
		free(list);
		if (buf == NULL)
			break;

		list = buf;
	}
}

void show_result(struct result_list *list)
{
	while(true) {
		printf("%d ", list->result);
		if (list->prev == NULL)
			break;
		list = list->prev;
	}
}

void draw_menu(void)
{
	printf("\033[2J\033[1;1H"); /* Clear screen */
	printf("d: throw dice\nr: show results:\nq: exit\n\n");
}

int main (void)
{
	int8_t ret = 0;
	struct result_list *list;
	time_t t;
	char buf;

	srand((unsigned) time(&t)); 

	list = malloc(sizeof(struct result_list));
	if (list == NULL) {
		printf("Out of memory\n");
		return -1;
	}
	list->result = rand() % 6 + 1;

	draw_menu();
	while(true) {
		if (buf != '\n')
			printf("\n\n$");
		buf = getchar();
		if (buf != '\n')
			draw_menu();

		switch (buf) {
		case 'q':
			goto exit;	
		case 'd':
			if (alloc_next_item(list) != ERR_OK) {
				printf("Failed to create next item list\n");
				goto exit;
			}
			list = list->next;
			list->result = rand() % 6 + 1;
			
			printf("%d", list->result);
			break;
		case 'r':
			show_result(list);
			break;
		default:
			break;	
		}

	}

exit:
	free_list(list);
	printf("\033[2J\033[1;1H"); /* Clear screen */

	return ret;
}















