#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

void print_help(void)
{
	printf("pak - pack files\n");
}

int main(int argc, char *argv[])
{
	char *filein, *fileout;
	FILE *openin, *openout;

	size_t read_size;
	char *buf;
	char symbol;
	char counter = 0;

	switch (argc) {
	case 1:
		print_help();
		return 0;
	default:
	case 3:
		fileout = argv[2];
	case 2:
		filein = argv[1];
	}

	if (argc == 2) {
		fileout = (char *) malloc(strlen(filein) + 4);
		strcpy(fileout, filein);
		strcat(fileout, ".pak");
	}

	openin = fopen(filein, "r");
	if (openin == NULL) {
		printf("Error: can not open input file\n");
		return 1;
	}

	openout = fopen(fileout, "w");
	if (openout == NULL) {
		printf("Error: can not open output file\n ");
		return 1;
	}

	buf = malloc(1);
	while(true) {
		read_size = fread(buf, 1, 1, openin);
		if (read_size == 0)
			break;

		if (symbol != *buf || counter == 255) {
			fwrite (&counter, 1, 1, openout);
			fwrite (&symbol, 1, 1, openout);
			counter = 0;
		}
		if (counter == 0)
			symbol = *buf;		
		if (symbol == *buf)
			counter++;

		//printf("%ls", (int *)buf);
	}
	free(buf);

	fclose(openin);
	fclose(openout);

	return 0;
}












