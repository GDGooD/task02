BUTTON_PIN=26

COUNT=0
STATUS=0
BUF=0

initialize () {
	if [ ! -f "/sys/class/gpio/gpio${BUTTON_PIN}" ]
	then
		echo ${BUTTON_PIN} > /sys/class/gpio/export
	fi  
	echo in > /sys/class/gpio/gpio${BUTTON_PIN}/direction
}

initialize

while true
do
	BUF=`cat /sys/class/gpio/gpio$BUTTON_PIN/value`
	if [ $BUF -ne $STATUS ]
	then
		STATUS=$BUF
		if [ $STATUS -eq 1 ]
		then
			COUNT=`expr $COUNT + 1`
			echo $COUNT
		fi
	fi
	sleep 0.05
done
