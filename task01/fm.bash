#!/bin/bash

CMD=0
INODE=0
FILE=0
LSLA=true
OUT=0

while true
do

	clear
	echo -e "\e[46m                         File Manager                          \e[0m"
	if $LSLA
	then
		ls -ila
	else
		ls -il
	fi


	echo -e "1:\e[46mshrt ls\e[0m2:\e[46mfull ls\e[0m3:\e[46mcd     \e[0m4:\e[46mcp     \e[0m5:\e[46mmv     \e[0m6:\e[46mrm     \e[0m0:\e[46mexit   \e[0m"
	read -n 1 -s -p "> " CMD


	case $CMD in
		0)
		clear
		exit
		;;

		1)
		echo "shrt ls"
		LSLA=false
		;;
		
		2)
		echo "full ls"
		LSLA=true
		;;
		
		3)
		echo "cd"
		read -p "Enter inode: " INODE
		cd "$(find .. -inum $INODE 2>/dev/null)"
		;;
		
		4)
		echo "cp"
		read -p "Enter inode: " INODE
		read -p "Enter file to paste: " FILE
		cp "$(find .. -inum $INODE 2>/dev/null)" $FILE
		;;
		
		5)
		echo "mv"
		read -p "Enter inode: " INODE
		read -p "Enter file to paste: " FILE
		mv "$(find .. -inum $INODE 2>/dev/null)" $FILE
		;;
		
		6)
		echo "rm"
		read -p "Enter inode: " INODE
		find ./ -inum $INODE -exec rm {} \;
		;;

		*)
		echo ""
		;;
	esac
done
