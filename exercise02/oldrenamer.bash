#!/bin/bash

for FILE in `find ./ -type f -name *54 -printf "%f\n"`
do	
	mv $FILE ~$FILE
	FOUND=1
done

if [[ $FOUND -eq 1 ]]
then
	../exercise01/rmtmp.bash
fi
