#!/bin/bash

HELP=false
RECURSIVE=false
CONFIRM=false
TEST=false
DIRECTORY='./'

for arg in $@
do
	case $arg in
		-h|--help)
		HELP=true
		;;

		-r|--recursive)
		RECURSIVE=true
		;;

		-y|--yes)
		CONFIRM=true
		;;

		-t|--test)
		TEST=true
		;;

		*)
		DIRECTORY=$arg
		;;
	esac
done


if $HELP
then
	echo "rmtmp - removes *.tmp, -*, _*, ~* files."
	echo ""
	echo "rmtmp [--help] [DIRECTORY]"
	exit
fi

rm -f $DIRECTORY/*.tmp
rm -f $DIRECTORY/-*
rm -f $DIRECTORY/_*
rm -f $DIRECTORY/~*




