#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>

MODULE_LICENSE("MIT");
MODULE_AUTHOR("Dmytro Kropenko");
MODULE_DESCRIPTION("UAH to EUR exchange");
MODULE_VERSION("0.01");

static const double concurrency_uah_eur = 32.057;

static int __init concur_init(void)
{
	printk(KERN_INFO "Welcome to DMYTROBANK\n");
	return 0;
}

static void __exit concur_exit(void)
{
	printk(KERN_INFO "---------------\n");
	printk(KERN_INFO "100.00 UAH = %d EUR\n", (int)(100/concurrency_uah_eur));
	printk(KERN_INFO "---------------\n");
}

module_init(concur_init);
module_exit(concur_exit);

