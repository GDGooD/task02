#!/bin/bash

rand() {
	return $((RANDOM % 6))
}

if (( $# == 0 ))
then
	read -p "Enter number: " UOUT
else
	UOUT=$1
fi

rand
ROUT=$?

if (( $ROUT > $UOUT ))
then
	echo "$ROUT is greater than $UOUT"
elif (( $ROUT < $UOUT ))
then
	echo "$ROUT is less than $UOUT"
else
	echo "$UOUT is equal to $UOUT"
fi
